package org.nr.tour.common.constant;

/**
 * @author chenhaiyang <690732060@qq.com>
 */
public interface PageConstants {
    String DEFAULT_PAGE_SIZE = "5";
    String DEFAULT_PAGE_NUMBER = "0";
}
