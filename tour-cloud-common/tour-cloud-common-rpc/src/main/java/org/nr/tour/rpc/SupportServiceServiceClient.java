package org.nr.tour.rpc;

import org.nr.tour.common.base.AbstractServiceDefinition;
import org.nr.tour.common.constant.ServiceConstants;
import org.nr.tour.domain.SupportService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author chenhaiyang <690732060@qq.com>
 */
@FeignClient(value = ServiceConstants.HOTEL_SERVICE, path = ServiceConstants.PATH_SERVICE)
public interface SupportServiceServiceClient extends AbstractServiceDefinition<SupportService, String> {

}
