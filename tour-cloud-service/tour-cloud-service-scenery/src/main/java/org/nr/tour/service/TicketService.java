package org.nr.tour.service;

import org.nr.tour.common.base.BaseService;
import org.nr.tour.domain.Ticket;

import java.util.List;

/**
 * @author chenhaiyang <690732060@qq.com>
 */
public interface TicketService extends BaseService<Ticket> {
    List<Ticket> getBySceneryId(String sceneryId);
}
