package org.nr.tour.service;

import org.nr.tour.common.base.BaseOrderService;
import org.nr.tour.domain.TicketOrder;
import org.nr.tour.domain.TicketOrderVisitor;

import java.util.List;

/**
 * @author chenhaiyang <690732060@qq.com>
 */
public interface TicketOrderService extends BaseOrderService<TicketOrder> {
    void updateVisitors(String orderId, List<TicketOrderVisitor> visitors);
}
